import { makeExecutableSchema } from '@graphql-tools/schema';
import { resolvers } from './resolvers'

const typeDefs = `

type Query {
    hello: String
    greet(name: String!): String
    pokemons: [Pokemon]
    getTrainers: [Trainer]
}

type Pokemon {
    _id: ID
    name: String!
    attack: String!
    ps: Int!
}

type Trainer {
    _id: ID
    name: String!
    mote: String!
    age: Int!
}

type Mutation {
    registerPkm ( input: PokemonInput ): Pokemon
    registerTrainer ( input: TrainerInput ): Trainer
    updateTrainer ( _id: ID, input: TrainerInput ): Trainer
    deleteTrainer ( _id: ID ): Trainer
}

input PokemonInput {
    name: String!
    attack: String!
    ps: Int!
}

input TrainerInput {
    name: String!
    mote: String!
    age: Int!
}

`;

export default makeExecutableSchema({
    typeDefs: typeDefs,
    resolvers: resolvers
});