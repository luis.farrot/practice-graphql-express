import mongoose from "mongoose";


export async function connect() {
    try {
        await mongoose.connect('mongodb://localhost/mongodbgraphql', {
            useNewUrlParser: true
        });
        console.log('>>> DB is connect!!');
    } catch (e) {
        console.log('Something goes wrong!!');
        console.log(e);
    }
}
