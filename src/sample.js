export const pokemons = [

    {
        _id: 0,
        name: 'Pikachu',
        attack: 'Impact Thunder',
        ps: 5
    },
    {
        _id: 1,
        name: 'Froakie',
        attack: 'Water Gun',
        ps: 5
    },
    {
        _id: 2,
        name: 'Noivern',
        attack: 'Dragon Pulse',
        ps: 5
    },
    {
        _id: 3,
        name: 'Lucario',
        attack: 'Aura Sphere',
        ps: 5
    },
    {
        _id: 4,
        name: 'Charizard',
        attack: 'Flamethrower',
        ps: 5
    },
    {
        _id: 5,
        name: 'Treecko',
        attack: 'Absorb',
        ps: 5
    },

];