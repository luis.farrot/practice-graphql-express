import { pokemons } from "./sample";

import Trainer from "./models/Trainer";

export const resolvers = {

    Query: {
        hello: () => 'Hello World GraphQL',
        greet(root, { name }) {
            console.log(name);
            return `Hello ${name}!!`;
        },
        pokemons() {
            return pokemons;
        },
        async getTrainers() {
            return await Trainer.find();
        }
    },

    Mutation: {

        registerPkm(_, { input }) {
            input._id = pokemons.length;
            pokemons.push(input);
            return input;
        },


        async registerTrainer(_, { input }) {
            const newTrainer = new Trainer(input);
            await newTrainer.save();
            return newTrainer;
        },

        async updateTrainer(_, { _id, input }) {
            return await Trainer.findByIdAndUpdate(_id, input, { new: true });
        },

        async deleteTrainer(_, { _id }) {
            return await Trainer.findByIdAndDelete(_id);
        },
    }

}