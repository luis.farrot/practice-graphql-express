import { Schema, model } from "mongoose";

const trainerSchema = new Schema({

    name: {
        type: String,
        required: true
    },

    mote: {
        type: String,
        required: true
    },

    age: {
        type: Number,
        required: true
    },

});

export default model('Trainer', trainerSchema);